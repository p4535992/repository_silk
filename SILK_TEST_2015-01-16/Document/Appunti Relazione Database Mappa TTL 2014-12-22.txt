   prefix:gr          baseURI: http://purl.org/goodrelations/v1#
   
   Nome della colonna del database       Campo con cui � stato mappato             Problema con la scelta del campo
    
************************   
***gr:BusinessEntity1***
************************                   
  `doc_id` int(11)                            ---------
  `url` varchar(255)                          schema:url                          
  `regione` varchar(50)                       ----------
  `provincia` varchar(50)                     schema:addressRegion
  `city` varchar(50)                          schema:addressLocality
  `indirizzo` varchar(255)                    ----------------------  schema:streetAddress 
  `iva` varchar(50)                           gr:vatID
  `email` varchar(255)                        schema:email
  `telefono` varchar(255)                     schema:telephone
  `fax` varchar(255)                          schema:faxNumber
  `edificio` varchar(1000)                    gr:legalName                             
  `nazione` varchar(50)                       schema:addressCountry
  `identifier` varchar(1000)                  URI della classe (Valore Key abilitato)  
  `description` varchar(5000)                 -----------
  `postalCode` varchar(5000)                  schema:postalCode
  `indirizzoNoCAP` varchar(5000)              schema:streetAddress 
[collegamento automatico di Karma con la relazione gr:hasPOS tra gr:BusinessEntity e gr:Location ]	 
	 
**************************
***gr:Location************
**************************  
  
  `latitude` varchar(255)                     schema:latitude
  `longitude` varchar(255)                    schema:longitude 
  `name_location` varchar(1000)               URI della classe (Valore key disabilitato)
  
  

	

