@Echo OFF
CD "%~dp0
ECHO Launch dir: "%~dp0"
SET TEST=km4c-InfoDoc_M-JARO_V-CITY-ADDRESS
rem km4c-InfoDoc_M-JARO-LEVI_V-CITY-ADDRESS-NAME_H
rem km4c-InfoDoc_M-JARO-LEVI_V-CITY-ADDRESS-COORD_G
rem km4c-InfoDoc_M-JARO-LEVI_V-CITY-ADDRESS-COORD_F
rem km4c-InfoDoc_M-JARO-LEVENSHTEINDISTANCE_V-CITY-ADDRESS
rem km4c-InfoDoc_M-JARO-LEVI_V-CITY-ADDRESS-COORD
rem km4c-InfoDoc_M-JARO-LEVI_V-CITY-ADDRESS_G
rem km4c-InfoDoc_M-JARO-LEVI_V-CITY-ADDRESS_F
rem km4c-InfoDoc_M-LEVI_V-CITY-ADDRESS
rem km4c-InfoDoc_M-JARO-LEVI_V-CITY-ADDRESS
rem km4c-InfoDoc_M-JARO_V-CITY-ADDRESS
rem km4c-InfoDoc_M-JAROWINKLER_V-CITY-ADDRESS
ECHO TEST: "%TEST%"
SET SILKPATH="%~dp0silk_2.6.0
rem PUSHD ".\km4c-InfoDoc_M-JARO-LEVENSHTEINDISTANCE_V-CITY-ADDRESS"
rem ECHO SILKPATH dir: %SILKPATH%
rem java -Xmx5G -DconfigFile="%CD%\km4c-InfoDoc_M-JARO-LEVENSHTEINDISTANCE_V-CITY-ADDRESS\SLS__km4c-InfoDoc_M-JARO-LEVENSHTEINDISTANCE_V-CITY-ADDRESS.xml" -DlinkSpec="interlink_location" -DlogQueries=true -Dreload=true -jar %CD%\silk_2.6.0\commandline\silk.jar
rem ECHO SLSPATH: "%CD%\%TEST%\SLS_"%TEST%".xml"

java -Xmx6G -DconfigFile="%CD%\%TEST%\SLS_"%TEST%".xml" -DlinkSpec="interlink_location" -DlogQueries=true -Dreload=true -jar %SILKPATH%\commandline\silk.jar
PAUSE&EXIT