Silk Link Discovery Framework
----------------------------

The Silk Link Discovery Framework is a suite of tools for discovering relationships between data items within different Linked Data sources.
Data publishers can use Silk to set RDF links from their data sources to other data sources on the Web.

More information about Silk can be found on:

http://silk.wbsg.de.

------------------------------
Silk Workbench
------------------------------

Silk Workbench is a web application which guides the user through the process of interlinking different data sources. 
For beginners, this is the main entry point for getting to know Silk.

------------------------------
Silk Command Line Applications
------------------------------

In addition to the Workbench, Silk provides three different command line applications for executing link specifications:
 - Silk Single Machine is used to generate RDF links on a single machine.
 - Silk MapReduce is used to generate RDF links between data sets using a cluster of multiple machines.
 - Silk Server can be used as an identity resolution component within applications that consume Linked Data from the Web.

 -----------------------------
Silk Free Text Preprocessor
------------------------------

The main goal of the Free Text Pre-processing tool is to produce a structured representation of data that contains or is derived from free text. The tool takes as input an RDF file with properties with free text values and an additional RDF file that contains structured data used to learn the extraction model. Based on the learned model the tool extracts new property-value pairs from free text. The resulting output is an RDF dump file containing the extracted structured values. Using a declarative XML-based language, a user can specify which extraction methods to use. 

